# 54gene bioinformatics training

To access this training in an interactive environment in your browser, click here:  
[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/data-analysis5%2F54gene_bioinf_tutorials/HEAD)

## Part 1: Intro to CLI
